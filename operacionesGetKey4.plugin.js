'use strict';

const request = require('request'),
 querystring = require('querystring'),
        _ = require('lodash');

delete require.cache[require.resolve('./operacionesLib.js')];
var lib = require('./operacionesLib.js');

/**************
INFO
pasamos los datos de key1 y key1_senior para determinar el key1 final
***************/

function getKey4(req, res) {
	console.log('invocado fichero operacionesGetSBKey4.plugin.js');

	var array_salida = [];
	var key4F = lib.obtener_key4_code(req.query.cod_key1, req.query.cod_key2, req.query.key3);

	array_salida.push({
			salida: key4F
		});
		
	res.jsonp(array_salida);	
}

module.exports = function (app, knex, auth) {
    app.route('/plugins/operacionesGetKey4')
            .get(auth.requiresLogin,  getKey4);
};