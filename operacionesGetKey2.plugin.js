'use strict';


const request = require('request'),
 querystring = require('querystring'),
        _ = require('lodash');
		
delete require.cache[require.resolve('./operacionesLib.js')];
var lib = require('./operacionesLib.js');

/**************
INFO
pasamos los datos de key1 y key1_senior para determinar el key1 final
***************/

function getKey2(req, res) {
	console.log('invocado fichero operacionesGetKey2.plugin.js');

	var array_salida = [];
	var key2_code = lib.obtener_key2_code(req.query.cod_key1, req.query.key2);

	array_salida.push({
			salida: key2_code
		});
		
	res.jsonp(array_salida);	
}

module.exports = function (app, knex, auth) {
    app.route('/plugins/operacionesGetKey2')
            .get(auth.requiresLogin,  getKey2);
};