'use strict';


const request = require('request'),
 querystring = require('querystring'),
        _ = require('lodash');

delete require.cache[require.resolve('./operacionesLib.js')];
var lib = require('./operacionesLib.js');

/**************
INFO
pasamos los datos de key1 y key1_senior para determinar el key1 final
***************/

function getKey1(req, res) {
	console.log('invocado fichero operacionesGetKey1.plugin.js');
	
	var array_salida = [];
	var key1_code = lib.obtener_key1_code(req.query.desc);

	array_salida.push({
			salida: key1_code
		});
		
	res.jsonp(array_salida);	
}

module.exports = function (app, knex, auth) {
    app.route('/plugins/operacionesGetKey1')
            .get(auth.requiresLogin,  getKey1);
};