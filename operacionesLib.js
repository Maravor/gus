module.exports = {

	obtener_key1_code: function (des_key1)
	{
		var cod_key1 = '';
		
		// INICIO RBE
		
		// INBOUND		
		// Clasifica la llamada
		if (des_key1 == "Natural") { cod_key1="LEADN";} // ✔
		else if (des_key1 == "Campaña") { cod_key1="LEADC";}  // ✔
		else if (des_key1 == "Sticker RTC") { cod_key1="LEADRTC";}  // ✔
		else if (des_key1 == "NSR+") { cod_key1="LEADNSR";}  // ✔
		else if (des_key1 == "Cliente referido") { cod_key1="LEADRF";}  // ✔
		else if (des_key1 == "Seguimiento improcedentes") { cod_key1="LEADSI2";}  // ✔
		else if (des_key1 == "Telecierre") { cod_key1="LEADTIB";}  // ✔
		
			// Mail
			else if (des_key1 == "Mail Customer Care") { cod_key1="LEADMCU";}  // ✔
			else if (des_key1 == "Mail Call Center") { cod_key1="LEADMCA";} // ✔
			else if (des_key1 == "Mail Marketing Cliente") { cod_key1="LEADMM";} //✔
			else if (des_key1 == "Mail Otros") { cod_key1="LEADMO";} // ✔
			
			// Gestión mail
			else if (des_key1 == "Gestión Mail Cuenta") { cod_key1="GESMAIL1"; }  // ✔
			else if (des_key1 == "Gestión Mail Campaña") { cod_key1="GESMAIL2"; }  // ✔
			
			// Trámite de outbound
			else if (des_key1 == "TMK") { cod_key1="LEADTMK2";} // ✔
			else if (des_key1 == "TMK - ZTC") { cod_key1="LEADTZ2";} // ✔
			else if (des_key1 == "ZTC") { cod_key1="LEADZTC2";}  // ✔
			else if (des_key1 == "Pilotos FUM") { cod_key1="LEAPFUM2";} // ✔
			else if (des_key1 == "Pilotos TMK") { cod_key1="LEAPTMK2";} // ✔
			else if (des_key1 == "Telecierre Inbound Pilotos") { cod_key1="LEADTIP2";} // ✔
			else if (des_key1 == "Seguimiento improcedente") { cod_key1="LEADSIO2";} // ✔
			else if (des_key1 == "Cliente referido TMK") { cod_key1="LEADRFT2";} // ✔
			else if (des_key1 == "Cliente referido ZTC") { cod_key1="LEADRFZ2";}	 // ✔
		
		// Llamada nueva
		else if (des_key1 == "LEADN-Llamada nueva") { cod_key1="LEADN";} // ✔
		else if (des_key1 == "LEADC-Llamada nueva") { cod_key1="LEADC";}  // ✔
		else if (des_key1 == "LEADRTC-Llamada nueva") { cod_key1="LEADRTC";}  // ✔
		else if (des_key1 == "LEADNSR-Llamada nueva") { cod_key1="LEADNSR";}  // ✔
		else if (des_key1 == "LEADMCU-Llamada nueva") { cod_key1="LEADMCU";}  // ✔
		else if (des_key1 == "LEADMCA-Llamada nueva") { cod_key1="LEADMCA";} // ✔
		else if (des_key1 == "LEADMM-Llamada nueva") { cod_key1="LEADMM";} // ✔
		else if (des_key1 == "LEADMO-Llamada nueva") { cod_key1="LEADMO";} // ✔
		else if (des_key1 == "LEADRF-Llamada nueva") { cod_key1="LEADRF";}  // ✔
		else if (des_key1 == "LEADTIB-Llamada nueva") { cod_key1="LEADTIB";}  // ✔
		
		// Seguimiento / PR
		else if (des_key1 == "LEADN-Seguimiento / PR") { cod_key1="LEADN2";} // ✔
		else if (des_key1 == "LEADC-Seguimiento / PR") { cod_key1="LEADC2";} // ✔
		else if (des_key1 == "LEADRTC-Seguimiento / PR") { cod_key1="LEADRTC2";} // ✔
		else if (des_key1 == "LEADNSR-Seguimiento / PR") { cod_key1="LEADNSR2";} // ✔
		else if (des_key1 == "LEADMCU-Seguimiento / PR") { cod_key1="LEADMCU2";} // ✔
		else if (des_key1 == "LEADMCA-Seguimiento / PR") { cod_key1="LEADMCA2";} // ✔
		else if (des_key1 == "LEADMM-Seguimiento / PR") { cod_key1="LEADMM2";} // ✔
		else if (des_key1 == "LEADMO-Seguimiento / PR") { cod_key1="LEADMO2";} // ✔
		else if (des_key1 == "LEADRF-Seguimiento / PR") { cod_key1="LEADRF2";} // ✔
		else if (des_key1 == "LEADSI2-Seguimiento / PR") { cod_key1="LEADSI2";}  // ✔
		else if (des_key1 == "LEADTIB-Seguimiento / PR") { cod_key1="LEADTIB2";}  // ✔
		
		// Improcedente 
		else if (des_key1 == "LEADN-Improcedente") { cod_key1="LEADN5";} // ✔
		else if (des_key1 == "LEADC-Improcedente") { cod_key1="LEADC5";} // ✔
		else if (des_key1 == "LEADRTC-Improcedente") { cod_key1="LEADRTC5";} // ✔
		else if (des_key1 == "LEADNSR-Improcedente") { cod_key1="LEADNSR5";} // ✔
		else if (des_key1 == "LEADMCU-Improcedente") { cod_key1="LEADMCU5";} // ✔
		else if (des_key1 == "LEADMCA-Improcedente") { cod_key1="LEADMCA5";} // ✔
		else if (des_key1 == "LEADMM-Improcedente") { cod_key1="LEADMM5";} // ✔
		else if (des_key1 == "LEADMO-Improcedente") { cod_key1="LEADMO5";} // ✔
		else if (des_key1 == "LEADRF-Improcedente") { cod_key1="LEADRF5";} // ✔
		else if (des_key1 == "LEADSI2-Improcedente") { cod_key1="LEADSI5";} // ✔
		else if (des_key1 == "LEADTIB-Improcedente") { cod_key1="LEADTIB5";} // ✔
		
		// Trámite
		else if (des_key1 == "LEADN-Trámite") { cod_key1="LEADN2";} // ✔
		else if (des_key1 == "LEADC-Trámite") { cod_key1="LEADC2";} // ✔
		else if (des_key1 == "LEADRTC-Trámite") { cod_key1="LEADRTC2";} // ✔
		else if (des_key1 == "LEADNSR-Trámite") { cod_key1="LEADNSR2";} // ✔
		else if (des_key1 == "LEADMCU-Trámite") { cod_key1="LEADMCU2";} // ✔
		else if (des_key1 == "LEADMCA-Trámite") { cod_key1="LEADMCA2";} // ✔
		else if (des_key1 == "LEADMM-Trámite") { cod_key1="LEADMM2";} // ✔
		else if (des_key1 == "LEADMO-Trámite") { cod_key1="LEADMO2";} // ✔
		else if (des_key1 == "LEADRF-Trámite") { cod_key1="LEADRF2";} // ✔
		else if (des_key1 == "LEADSI2-Trámite") { cod_key1="LEADSI2";}  // ✔
		else if (des_key1 == "LEADTIB-Trámite") { cod_key1="LEADTIB2";}  // ✔
		
		// OUTBOUND
		// Clasifica la llamada
		else if (des_key1 == "O-TMK") { cod_key1="LEADTMK";} // 
		else if (des_key1 == "O-TMK - ZTC") { cod_key1="LEADTZ";} // 
		else if (des_key1 == "O-ZTC") { cod_key1="LEADZTC";} // 
		else if (des_key1 == "O-Pilotos FUM") { cod_key1="LEAPFUM";} // 
		else if (des_key1 == "O-Pilotos TMK") { cod_key1="LEADPTMK";} // 
		else if (des_key1 == "O-Telecierre Inbound Pilotos") { cod_key1="LEADTIP";} // 
		else if (des_key1 == "O-Seguimiento improcedentes") { cod_key1="LEADSIO2";} // 
		else if (des_key1 == "O-Cliente Referido TMK") { cod_key1="LEADRFT";} // 
		else if (des_key1 == "O-Cliente Referido ZTC") { cod_key1="LEADRFZ";} // 
		
			// Telecierre
			else if (des_key1 == "OT-TMK") { cod_key1="LEADTMK3";} // 
			else if (des_key1 == "OT-TMK - ZTC") { cod_key1="LEADTZ3";} // 
			else if (des_key1 == "OT-ZTC") { cod_key1="LEADZTC3";} // 
			else if (des_key1 == "OT-Pilotos FUM") { cod_key1="LEAPFUM3";} // 
			else if (des_key1 == "OT-Pilotos TMK") { cod_key1="LEADPTMK3";} // 
			else if (des_key1 == "OT-Telecierre Inbound Pilotos") { cod_key1="LEADTIP3";} // 
			else if (des_key1 == "OT-Cliente Referido TMK") { cod_key1="LEADRFT3";} // 
			else if (des_key1 == "OT-Cliente Referido ZTC") { cod_key1="LEADRFZ3";} // 
			
				// Llamada nueva
				else if (des_key1 == "LEADTMK3-Llamada nueva") { cod_key1="LEADTMK3";} // 
				else if (des_key1 == "LEADTZ3-Llamada nueva") { cod_key1="LEADTZ3";} // 
				else if (des_key1 == "LEADZTC3-Llamada nueva") { cod_key1="LEADZTC3";} // 
				else if (des_key1 == "LEAPFUM3-Llamada nueva") { cod_key1="LEAPFUM3";} // 
				else if (des_key1 == "LEADPTMK3-Llamada nueva") { cod_key1="LEADPTMK3";} // 
				else if (des_key1 == "LEADTIP3-Llamada nueva") { cod_key1="LEADTIP3";} // 
				else if (des_key1 == "LEADRFT3-Llamada nueva") { cod_key1="LEADRFT3";} // 
				else if (des_key1 == "LEADRFZ3-Llamada nueva") { cod_key1="LEADRFZ3";} // 
				
				// Seguimiento / PR
				else if (des_key1 == "LEADTMK3-Seguimiento / PR") { cod_key1="LEADTMK4";} // 
				else if (des_key1 == "LEADTZ3-Seguimiento / PR") { cod_key1="LEADTZ4";} // 
				else if (des_key1 == "LEADZTC3-Seguimiento / PR") { cod_key1="LEADZTC4";} // 
				else if (des_key1 == "LEAPFUM3-Seguimiento / PR") { cod_key1="LEAPFUM4";} // 
				else if (des_key1 == "LEADPTMK3-Seguimiento / PR") { cod_key1="LEAPTMK4";} // 
				else if (des_key1 == "LEADTIP3-Seguimiento / PR") { cod_key1="LEADTIP4";} // 
				else if (des_key1 == "LEADRFT3-Seguimiento / PR") { cod_key1="LEADRFT4";} // 
				else if (des_key1 == "LEADRFZ3-Seguimiento / PR") { cod_key1="LEADRFZ4";} // 
				
				// Improcedente
				else if (des_key1 == "LEADTMK3-Improcedente") { cod_key1="LETTMK5";} // 
				else if (des_key1 == "LEADTZ3-Improcedente") { cod_key1="LETTZ5";} // 
				else if (des_key1 == "LEADZTC3-Improcedente") { cod_key1="LETZTC5";} // 
				else if (des_key1 == "LEAPFUM3-Improcedente") { cod_key1="LETPFUM5";} // 
				else if (des_key1 == "LEADPTMK3-Improcedente") { cod_key1="LETPTMK5";} // 
				else if (des_key1 == "LEADTIP3-Improcedente") { cod_key1="LEADTIP5";} // 
				else if (des_key1 == "LEADRFT3-Improcedente") { cod_key1="LETRFT5";} // 
				else if (des_key1 == "LEADRFZ3-Improcedente") { cod_key1="LETRFZ5";} // 
				
				// Trámite
				else if (des_key1 == "LEADTMK3-Trámite") { cod_key1="LEADTMK4";} // 
				else if (des_key1 == "LEADTZ3-Trámite") { cod_key1="LEADTZ4";} // 
				else if (des_key1 == "LEADZTC3-Trámite") { cod_key1="LEADZTC4";} // 
				else if (des_key1 == "LEAPFUM3-Trámite") { cod_key1="LEAPFUM4";} // 
				else if (des_key1 == "LEADPTMK3-Trámite") { cod_key1="LEAPTMK4";} // 
				else if (des_key1 == "LEADTIP3-Trámite") { cod_key1="LEADTIP4";} // 
				else if (des_key1 == "LEADRFT3-Trámite") { cod_key1="LEADRFT4";} // 
				else if (des_key1 == "LEADRFZ3-Trámite") { cod_key1="LEADRFZ4";} // 		
		
		// Llamada nueva
		else if (des_key1 == "LEADTMK-Llamada nueva") { cod_key1="LEADTMK";} // 
		else if (des_key1 == "LEADTZ-Llamada nueva") { cod_key1="LEADTZ";} // 
		else if (des_key1 == "LEADZTC-Llamada nueva") { cod_key1="LEADZTC";} // 
		else if (des_key1 == "LEAPFUM-Llamada nueva") { cod_key1="LEAPFUM";} // 
		else if (des_key1 == "LEADPTMK-Llamada nueva") { cod_key1="LEADPTMK";} // 
		else if (des_key1 == "LEADTIP-Llamada nueva") { cod_key1="LEADTIP";} // 
		else if (des_key1 == "LEADRFT-Llamada nueva") { cod_key1="LEADRFT";} // 
		else if (des_key1 == "LEADRFZ-Llamada nueva") { cod_key1="LEADRFZ";} // 
		
		// Seguimiento / PR
		else if (des_key1 == "LEADTMK-Seguimiento / PR") { cod_key1="LEADTMK2";} // 
		else if (des_key1 == "LEADTZ-Seguimiento / PR") { cod_key1="LEADTZ2";} // 
		else if (des_key1 == "LEADZTC-Seguimiento / PR") { cod_key1="LEADZTC2";} // 
		else if (des_key1 == "LEAPFUM-Seguimiento / PR") { cod_key1="LEAPFUM2";} // 
		else if (des_key1 == "LEADPTMK-Seguimiento / PR") { cod_key1="LEAPTMK2";} // 
		else if (des_key1 == "LEADTIP-Seguimiento / PR") { cod_key1="LEADTIP2";} // 
		else if (des_key1 == "LEADSIO2-Seguimiento / PR") { cod_key1="LEADSIO2";} // 
		else if (des_key1 == "LEADRFT-Seguimiento / PR") { cod_key1="LEADRFT2";} // 
		else if (des_key1 == "LEADRFZ-Seguimiento / PR") { cod_key1="LEADRFZ2";} // 

		// Improcedente
		else if (des_key1 == "LEADTMK-Improcedente") { cod_key1="LEADTMK5";} // 
		else if (des_key1 == "LEADTZ-Improcedente") { cod_key1="LEADTZ5";} // 
		else if (des_key1 == "LEADZTC-Improcedente") { cod_key1="LEADZTC5";} // 
		else if (des_key1 == "LEAPFUM-Improcedente") { cod_key1="LEAPFUM5";} // 
		else if (des_key1 == "LEADPTMK-Improcedente") { cod_key1="LEAPTMK5";} // 
		else if (des_key1 == "LEADTIP-Improcedente") { cod_key1="LEADTIP5";} // 
		else if (des_key1 == "LEADSIO2-Improcedente") { cod_key1="LEADSIO5";} // 
		else if (des_key1 == "LEADRFT-Improcedente") { cod_key1="LEADRFT5";} // 
		else if (des_key1 == "LEADRFZ-Improcedente") { cod_key1="LEADRFZ5";} // 
		
		// Trámite
		else if (des_key1 == "LEADTMK-Trámite") { cod_key1="LEADTMK2";} // 
		else if (des_key1 == "LEADTZ-Trámite") { cod_key1="LEADTZ2";} // 
		else if (des_key1 == "LEADZTC-Trámite") { cod_key1="LEADZTC2";} // 
		else if (des_key1 == "LEAPFUM-Trámite") { cod_key1="LEAPFUM2";} // 
		else if (des_key1 == "LEADPTMK-Trámite") { cod_key1="LEAPTMK2";} // 
		else if (des_key1 == "LEADTIP-Trámite") { cod_key1="LEADTIP2";} // 
		else if (des_key1 == "LEADSIO2-Trámite") { cod_key1="LEADSIO2";} // 
		else if (des_key1 == "LEADRFT-Trámite") { cod_key1="LEADRFT2";} // 
		else if (des_key1 == "LEADRFZ-Trámite") { cod_key1="LEADRFZ2";} // 
		
		// FIN KEYS INBOUND/OUTBOUND RBE
		
		// INICIO UPSELL
		
		// EXTENSIONS Y UPG
		// Llamada nueva
		else if (des_key1 == "Extensions") { cod_key1="U01"; }
		else if (des_key1 == "Extensions C. Care") { cod_key1="U01_1"; }
		else if (des_key1 == "Extensions Seg. Improcedentes") { cod_key1="U01_2"; }
		else if (des_key1 == "Extensions PR+ (quitarlo)") { cod_key1="U01_3"; }
		else if (des_key1 == "Extensions Zero Vision") { cod_key1="U01_ZV"; }
		else if (des_key1 == "Extensions Max 3") { cod_key1="U01_4"; }
		else if (des_key1 == "Extensions Black Friday") { cod_key1="U01_BF"; }
		else if (des_key1 == "Extensions Comerciales") { cod_key1="U01_5"; }
		else if (des_key1 == "Extensions Sustitución Ampliaciones") { cod_key1="U01_6"; }
		else if (des_key1 == "Extensions Sustitución Central") { cod_key1="U01_7"; }
		else if (des_key1 == "Ext. Ingleses C. Care") { cod_key1="U01_9"; }
		else if (des_key1 == "Extensions Natural Acelerator") { cod_key1="U01_10"; }
		else if (des_key1 == "Extensions Campañas Acelerator") { cod_key1="U01_13"; }
		else if (des_key1 == "Tienda OnLine Acelerator") { cod_key1="U01_14"; }
		else if (des_key1 == "Upgrade") { cod_key1="U02"; }
		else if (des_key1 == "Upgrade Forzado (quitarlo)") { cod_key1="U02F"; }
		else if (des_key1 == "Upgrade C. Care") { cod_key1="U02_1"; }
		else if (des_key1 == "Upgrade C. Care Forzado  (quitarlo)") { cod_key1="U02_1F"; }
		else if (des_key1 == "Upgrade Seg. Improcedentes") { cod_key1="U02_2"; }
		else if (des_key1 == "Upgrade PR+  (quitarlo)") { cod_key1="U02_3"; }
		else if (des_key1 == "Upgrade Max 3") { cod_key1="U02_4"; }
		else if (des_key1 == "Upgrade Outsourcing") { cod_key1="U02_5"; }
		else if (des_key1 == "Upgrade Natural Acelerator") { cod_key1="U02_10"; }
		else if (des_key1 == "Telecierre Extensions Campaña") { cod_key1="U01_TC"; }
		else if (des_key1 == "Telecierre Extensions PR") { cod_key1="U01_1T"; }
		else if (des_key1 == "Telecierre Extensions NoAcepta") { cod_key1="U01_2T"; }
		else if (des_key1 == "Telecierre Extensions Mto. Finalizados") { cod_key1="U01_3T"; }
		else if (des_key1 == "Telecierre Extensions Mto. Cerrados") { cod_key1="U01_4T"; }
		else if (des_key1 == "Telecierre Campaña Tienda Online") { cod_key1="U01_8T"; }
		else if (des_key1 == "Telecierre Extensions Black Friday") { cod_key1="U01_BFT"; }
		else if (des_key1 == "Telecierre Extensions Zero Vision") { cod_key1="U01_TZV"; }
		else if (des_key1 == "TELECIERRE EXTENSION FENIX") { cod_key1="FENIX1"; }
		else if (des_key1 == "TELECIERRE EXTENSION CAMPAÑA") { cod_key1="U01_TC"; }
		else if (des_key1 == "Telecierre Extensions Comerciales") { cod_key1="U01_5T"; }
		else if (des_key1 == "Telecierre Extensions Sustitución Ampliaciones") { cod_key1="U01_6T"; }
		else if (des_key1 == "Teleciere Extensions Sustitución Central") { cod_key1="U01_7T"; }
		else if (des_key1 == "Telecierre Monetización II") { cod_key1="U01_11T"; }
		else if (des_key1 == "Telecierre CoC Inglés") { cod_key1="U01_12T"; }
		else if (des_key1 == "Telecierre Upgrade PR") { cod_key1="U02_1T"; }
		else if (des_key1 == "Telecierre Upgrade Forzado PR (quitarlo)") { cod_key1="U02_1FT"; }
		else if (des_key1 == "Telecierre Upgrade NoAcepta") { cod_key1="U02_2T"; }
		else if (des_key1 == "Telecierre Upgrade Forzado NoAcepta (quitarlo)") { cod_key1="U02_2FT"; }
		else if (des_key1 == "Telecierre Upgrade Mto. Finalizados") { cod_key1="U02_3T"; }
		else if (des_key1 == "Telecierre Upgrade Forzado Mto. Finalizados (quitarlo)") { cod_key1="U02_3FT"; }
		else if (des_key1 == "Telecierre Upgrade Mto. Cerrados") { cod_key1="U02_4T"; }
		else if (des_key1 == "Telecierre Upgrade Forzado Mto. Cerrados (quitarlo)") { cod_key1="U02_4FT"; }
		else if (des_key1 == "Telecierre Upgrade Outsourcing") { cod_key1="U02_5T"; }
		
		// Seguimiento
		else if (des_key1 == "Seguimiento Extensions") { cod_key1="U01S"; }
		else if (des_key1 == "Seguimiento Extensions C. Care") { cod_key1="U01_1S"; }
		else if (des_key1 == "Seguimiento Ext. Ingleses C. Care") { cod_key1="U01_9S"; }
		else if (des_key1 == "Seguimiento Extensions Natural Acelerator") { cod_key1="U01_10S"; }
		else if (des_key1 == "Seguimiento Extensions Campañas Acelerator") { cod_key1="U01_13S"; }
		else if (des_key1 == "Seguimiento Tienda OnLine Acelerator") { cod_key1="U01_14S"; }
		else if (des_key1 == "Seguimiento Extensions Seg. Improcedentes") { cod_key1="U01_2S"; }
		else if (des_key1 == "Seguimiento Extensions PR+") { cod_key1="U01_3S"; }
		else if (des_key1 == "Seguimiento Extensions Zero Vision") { cod_key1="U01_ZVS"; }
		else if (des_key1 == "Seguimiento Upgrade") { cod_key1="U02S"; }
		else if (des_key1 == "Seguimiento Upgrade Forzado (forzado)") { cod_key1="U02FS"; }
		else if (des_key1 == "Seguimiento Upgrade C. Care") { cod_key1="U02_1S"; }
		else if (des_key1 == "Seguimiento Extensions Max 3") { cod_key1="U01_4S"; }
		else if (des_key1 == "Seguimiento Telecierre Campaña Tienda Online") { cod_key1="U01_8TS"; }
		else if (des_key1 == "Seguimiento Extensions Black Friday") { cod_key1="U01_BFS"; }
		else if (des_key1 == "Seguimiento Extensions Comerciales") { cod_key1="U01_5S"; }
		else if (des_key1 == "Seguimiento Extensions Sustitución Ampliaciones") { cod_key1="U01_6S"; }
		else if (des_key1 == "Seguimiento Extensions Sustitución Central") { cod_key1="U01_7S"; }
		else if (des_key1 == "Seguimiento Telecierre Monetización II") { cod_key1="U01_11TS"; }
		else if (des_key1 == "Seguimiento Telecierre CoC Inglés") { cod_key1="U01_12TS"; }
		else if (des_key1 == "Seguimiento Upgrade C. Care Forzado (quitarlo)") { cod_key1="U02_1FS"; }
		else if (des_key1 == "Seguimiento Upgrade Seg. Improcedentes") { cod_key1="U02_2S"; }
		else if (des_key1 == "Seguimiento Upgrade PR+") { cod_key1="U02_3S"; }
		else if (des_key1 == "Seguimiento Upgrade Max 3") { cod_key1="U02_4S"; }
		else if (des_key1 == "Seguimiento Upgrade Outsourcing") { cod_key1="U02_5S"; }
		else if (des_key1 == "Seguimiento Upgrade Natural Acelerator") { cod_key1="U02_10S"; }
		else if (des_key1 == "Seguimiento Telecierre Extensions Campaña") { cod_key1="U01_TCS"; }
		else if (des_key1 == "Seguimiento Telecierre Extensions PR") { cod_key1="U01_1TS"; }
		else if (des_key1 == "Seguimiento Telecierre Extensions NoAcepta") { cod_key1="U01_2TS"; }
		else if (des_key1 == "Seguimiento Telecierre Extensions Mto. Finalizados") { cod_key1="U01_3TS"; }
		else if (des_key1 == "Seguimiento Telecierre Extensions Mto. Cerrados") { cod_key1="U01_4TS"; }
		else if (des_key1 == "Seguimiento Telecierre Zero Vision") { cod_key1="U01_STZV"; }
		else if (des_key1 == "SEGUIMIENTO TELECIERRE EXTENSION FENIX") { cod_key1="FENIX2"; }
		else if (des_key1 == "SEGUIMIENTO TELECIERRE EXTENSION CAMPAÑA") { cod_key1="U01_TCS"; }
		else if (des_key1 == "Seguimiento Telecierre Extensions Comerciales") { cod_key1="U01_5TS"; }
		else if (des_key1 == "Seguimiento Telecierre Extensions Sustitución Ampliaciones") { cod_key1="U01_6TS"; }
		else if (des_key1 == "Seguimiento Telecierre Extensions Sustitución Central") { cod_key1="U01_7TS"; }
		else if (des_key1 == "Seguimiento Telecierre Upgrade PR") { cod_key1="U02_1TS"; }
		else if (des_key1 == "Seguimiento Telecierre Upgrade Forzado PR (quitarlo)") { cod_key1="U02_1FTS"; }
		else if (des_key1 == "Seguimiento Telecierre Upgrade NoAcepta") { cod_key1="U02_2TS"; }
		else if (des_key1 == "Seguimiento Telecierre Upgrade Forzado NoAcepta (quitarlo)") { cod_key1="U02_2FTS"; }
		else if (des_key1 == "Seguimiento Telecierre Upgrade Mto. Finalizados") { cod_key1="U02_3TS"; }
		else if (des_key1 == "Seguimiento Telecierre Upgrade Forzado Mto. Finalizados (quitarlo)") { cod_key1="U02_3FTS"; }
		else if (des_key1 == "Seguimiento Telecierre Upgrade Mto. Cerrados") { cod_key1="U02_4TS"; }
		else if (des_key1 == "Seguimiento Telecierre Upgrade Forzado Mto. Cerrados (quitarlo)") { cod_key1="U02_4FTS"; }
		else if (des_key1 == "Seguimiento Telecierre Upgrade Outsourcing") { cod_key1="U02_5TS"; }
		
		// ANPS Y TRASLADOS
		// Llamada nueva
		else if (des_key1 == "Traslado") { cod_key1="U03"; }
		else if (des_key1 == "Traslado con Upgrade forzado") { cod_key1="U03F"; }
		else if (des_key1 == "Traslado C. Care") { cod_key1="U03_1"; }
		else if (des_key1 == "Traslado C. Care con Upgrade forzado") { cod_key1="U03_1F"; }
		else if (des_key1 == "ANP") { cod_key1="U04"; }
		else if (des_key1 == "ANP con Upgrade forzado") { cod_key1="U04F"; }
		else if (des_key1 == "ANP C. Care") { cod_key1="U04_1"; }
		else if (des_key1 == "ANP C. Care con Upgrade forzado") { cod_key1="U04_1F"; }
		else if (des_key1 == "Telecierre Traslado PR") { cod_key1="U03_1T"; }
		else if (des_key1 == "Telecierre Traslado NoAcepta") { cod_key1="U03_2T"; }
		else if (des_key1 == "Telecierre Traslado Mto. Finalizados") { cod_key1="U03_3T"; }
		else if (des_key1 == "Telecierre Traslado Mto. Cerrados") { cod_key1="U03_4T"; }
		else if (des_key1 == "Telecierre ANP PR") { cod_key1="U04_1T"; }
		else if (des_key1 == "Telecierre ANP NoAcepta") { cod_key1="U04_2T"; }
		else if (des_key1 == "Telecierre ANP Mto. Finalizados") { cod_key1="U04_3T"; }
		else if (des_key1 == "Telecierre ANP Mto. Cerrados") { cod_key1="U04_4T"; }
		
		// Seguimiento
		else if (des_key1 == "Seguimiento Traslado") { cod_key1="U03S"; }
		else if (des_key1 == "Seguimiento Traslado con Upgrade forzado") { cod_key1="U03FS"; }
		else if (des_key1 == "Seguimiento Traslado C. Care") { cod_key1="U03_1S"; }
		else if (des_key1 == "Seguimiento Traslado C. Care con Upgrade forzado") { cod_key1="U03_1FS"; }
		else if (des_key1 == "Seguimiento ANP") { cod_key1="U04S"; }
		else if (des_key1 == "Seguimiento ANP con Upgrade forzado") { cod_key1="U04FS"; }
		else if (des_key1 == "Seguimiento ANP C. Care") { cod_key1="U04_1S"; }
		else if (des_key1 == "Seguimiento ANP C. Care con Upgrade forzado") { cod_key1="U04_1FS"; }
		else if (des_key1 == "Seguimiento Telecierre Traslado PR") { cod_key1="U03_1TS"; }
		else if (des_key1 == "Seguimiento Telecierre Traslado NoAcepta") { cod_key1="U03_2TS"; }
		else if (des_key1 == "Seguimiento Telecierre Traslado Mto. Finalizados") { cod_key1="U03_3TS"; }
		else if (des_key1 == "Seguimiento Telecierre Traslado Mto. Cerrados") { cod_key1="U03_4TS"; }
		else if (des_key1 == "Seguimiento Telecierre ANP PR") { cod_key1="U04_1TS"; }
		else if (des_key1 == "Seguimiento Telecierre ANP NoAcepta") { cod_key1="U04_2TS"; }
		else if (des_key1 == "Seguimiento Telecierre ANP Mto. Finalizados") { cod_key1="U04_3TS"; }
		else if (des_key1 == "Seguimiento Telecierre ANP Mto. Cerrados") { cod_key1="U04_4TS"; }
		
		// EXCLUSIONES TELECIERRE
		else if (des_key1 == "Exclusión Telecierre ZeroVision") { cod_key1="U01_TZVE"; }
		else if (des_key1 == "Exclusión Telecierre FENIX") { cod_key1="FENIX1E"; }
		else if (des_key1 == "Exclusión Telecierre Campañas") { cod_key1="U01_TCE"; }
		else if (des_key1 == "Exclusión Telecierre Black Friday") { cod_key1="U01_BFTE"; }
		else if (des_key1 == "Exclusión Telecierre Campaña Tienda Online") { cod_key1="U01_8TE"; }
		else if (des_key1 == "Exclusión Telecierre Monetización II") { cod_key1="U01_11TE"; }
		else if (des_key1 == "Exclusión Telecierre CoC Inglés") { cod_key1="U01_12TE"; }
		else if (des_key1 == "Exclusion Telecierre Extensions PR") { cod_key1="U01_1TE"; }
		else if (des_key1 == "Exclusion Telecierre Extensions NoAcepta") { cod_key1="U01_2TE"; }
		else if (des_key1 == "Exclusion Telecierre Extensions Mto. Finalizados") { cod_key1="U01_3TE"; }
		else if (des_key1 == "Exclusion Telecierre Extensions Mto. Cerrados") { cod_key1="U01_4TE"; }
		else if (des_key1 == "Exclusion Telecierre Extensions Comerciales") { cod_key1="U01_5TE"; }
		else if (des_key1 == "Exclusión Telecierre Extensions Sustitución Ampliaciones") { cod_key1="U01_6TE"; }
		else if (des_key1 == "Exclusión Telecierre Extensions Sustitución Central") { cod_key1="U01_7TE"; }
		else if (des_key1 == "Exclusion Telecierre Upgrade PR") { cod_key1="U02_1TE"; }
		else if (des_key1 == "Exclusion Telecierre Upgrade NoAcepta") { cod_key1="U02_2TE"; }
		else if (des_key1 == "Exclusion Telecierre Upgrade Mto. Finalizados") { cod_key1="U02_3TE"; }
		else if (des_key1 == "Exclusion Telecierre Upgrade Mto. Cerrados") { cod_key1="U02_4TE"; }
		else if (des_key1 == "Exclusión Telecierre Upgrade Outsourcing") { cod_key1="U02_5TE"; }
		else if (des_key1 == "Exclusion Telecierre Upgrade Forzado PR (quitarlo)") { cod_key1="U02_1FTE"; }
		else if (des_key1 == "Exclusion Telecierre Upgrade Forzado NoAcepta (quitarlo)") { cod_key1="U02_2FTE"; }
		else if (des_key1 == "Exclusion Telecierre Upgrade Forzado Mto. Finalizados") { cod_key1="U02_3FTE"; }
		else if (des_key1 == "Exclusion Telecierre Upgrade Forzado Mto. Cerrados") { cod_key1="U02_4FTE"; }
		else if (des_key1 == "Exclusion Telecierre Traslado PR") { cod_key1="U03_1TE"; }
		else if (des_key1 == "Exclusion Telecierre Traslado NoAcepta") { cod_key1="U03_2TE"; }
		else if (des_key1 == "Exclusion Telecierre Traslado Mto. Finalizados") { cod_key1="U03_3TE"; }
		else if (des_key1 == "Exclusion Telecierre Traslado Mto. Cerrados") { cod_key1="U03_4TE"; }
		else if (des_key1 == "Exclusion Telecierre ANP PR") { cod_key1="U04_1TE"; }
		else if (des_key1 == "Exclusion Telecierre ANP NoAcepta") { cod_key1="U04_2TE"; }
		else if (des_key1 == "Exclusion Telecierre ANP Mto. Finalizados") { cod_key1="U04_3TE"; }
		else if (des_key1 == "Exclusion Telecierre ANP Mto. Cerrados") { cod_key1="U04_4TE"; }
		
		// IMPROCEDENTES
		// Improcedentes
		else if (des_key1 == "Improcedente") { cod_key1="UI"; }
		else if (des_key1 == "Improcedente C. Care") { cod_key1="UI_1"; }
		else if (des_key1 == "Improcedente DIY") { cod_key1="UI_05_6"; }
		
		// CAMPAÑAS
		// Llamada nueva
		else if (des_key1 == "SDI - Sistema de deteccion de incendios") { cod_key1="U05_1"; }
		else if (des_key1 == "Cloud Cam") { cod_key1="U05_2"; }
		else if (des_key1 == "On Road") { cod_key1="U05_3"; }
		else if (des_key1 == "Otras Campañas") { cod_key1="U05_4"; }
		else if (des_key1 == "Campañas de servicios") { cod_key1="U05_5"; }
		else if (des_key1 == "Campañas DIY") { cod_key1="U05_6"; }
		else if (des_key1 == "Campañas Zero Vision") { cod_key1="U05_7"; }
		else if (des_key1 == "Campaña Tienda Online Tienda online Pedido") { cod_key1="U05_8"; }
		else if (des_key1 == "Plataformas Externas") { cod_key1="U05_9"; }
		else if (des_key1 == "Sentinel 2.0") { cod_key1="U05_10"; }
		else if (des_key1 == "Coc + Crossell") { cod_key1="U05_11"; }
		else if (des_key1 == "Campaña Seguridad Exterior") { cod_key1="U05_12"; }
		else if (des_key1 == "Contacto Tienda Online Tienda Online Contacto dudas") { cod_key1="U05_13"; }
		else if (des_key1 == "Retarging") { cod_key1="U05_14"; }
		else if (des_key1 == "Retargeting Carrito") { cod_key1="U05_15"; }
		else if (des_key1 == "Tienda online carrito dudas") { cod_key1="U05_16"; }
		else if (des_key1 == "Tienda online visitas") { cod_key1="U05_17"; }
		else if (des_key1 == "LG Retargeting") { cod_key1="U05_18"; }
		else if (des_key1 == "Retargeting Check Out") { cod_key1="U05_19"; }
		
		// Seguimiento
		else if (des_key1 == "Seguimiento SDI - Sistema de deteccion de incendios") { cod_key1="U05_1S"; }
		else if (des_key1 == "Seguimiento Cloud Cam") { cod_key1="U05_2S"; }
		else if (des_key1 == "Seguimiento On Road") { cod_key1="U05_3S"; }
		else if (des_key1 == "Seguimiento Otras Campañas") { cod_key1="U05_4S"; }
		else if (des_key1 == "Seguimiento Campañas de servicios") { cod_key1="U05_5S"; }
		else if (des_key1 == "Seguimiento Campañas DIY") { cod_key1="U05_6S"; }
		else if (des_key1 == "Seguimiento Campañas Zero Vision") { cod_key1="U05_7S"; }
		else if (des_key1 == "Seguimiento Campaña Tienda Online Pedido") { cod_key1="U05_8S"; }
		else if (des_key1 == "Seguimiento Plataformas Externas") { cod_key1="U05_9S"; }
		else if (des_key1 == "Seguimiento Sentinel 2.0") { cod_key1="U05_10S"; }
		else if (des_key1 == "Seguimiento Coc + Crossell") { cod_key1="U05_11S"; }
		else if (des_key1 == "Seguimiento Campaña Seguridad Exterior") { cod_key1="U05_12S"; }
		else if (des_key1 == "Seguimiento Tienda Online Contacto dudas") { cod_key1="U05_13S"; }
		else if (des_key1 == "Seguimiento Retarging") { cod_key1="U05_14S"; }
		else if (des_key1 == "Seguimiento Retargeting Carrito") { cod_key1="U05_15S"; }
		else if (des_key1 == "Seguimiento Tienda online carrito dudas") { cod_key1="U05_16S"; }
		else if (des_key1 == "SeguimientoTienda online visitas") { cod_key1="U05_17S"; }
		else if (des_key1 == "Seguimiento LG Retargeting") { cod_key1="U05_18S"; }
		else if (des_key1 == "Seguimiento Retargeting Check Out") { cod_key1="U05_19S"; }
		
		// Exclusiones
		else if (des_key1 == "Exclusión Tienda Online") { cod_key1="U05_13E"; }
		else if (des_key1 == "Exclusión Retarging") { cod_key1="U05_14E"; }
		else if (des_key1 == "Exclusión  Retargeting Carrito") { cod_key1="U05_15E"; }
		else if (des_key1 == "Exclusión Retargeting Check Out") { cod_key1="U05_19E"; }
		
		// PILOTOS
		// PKMT
		else if (des_key1 == "Piloto 1") { cod_key1="U07_1"; }
		else if (des_key1 == "Piloto 2") { cod_key1="U07_2"; }
		else if (des_key1 == "Piloto 3") { cod_key1="U07_3"; }
		else if (des_key1 == "CoC") { cod_key1="U07_4"; }
		else if (des_key1 == "ZeroVision") { cod_key1="U07_5"; }
		else if (des_key1 == "SDI") { cod_key1="U07_6"; }
		else if (des_key1 == "MAX") { cod_key1="U07_7"; }
		else if (des_key1 == "Extensions") { cod_key1="U07_8"; }
		else if (des_key1 == "Segmentación") { cod_key1="U07_9"; }
		
		// ??
		else if (des_key1 == "Seguimiento Piloto 1") { cod_key1="U07_1S"; }
		else if (des_key1 == "Seguimiento Piloto 2") { cod_key1="U07_2S"; }
		else if (des_key1 == "Seguimiento Piloto 3") { cod_key1="U07_3S"; }
		else if (des_key1 == "Seguimiento CoC") { cod_key1="U07_4S"; }
		else if (des_key1 == "Seguimiento ZeroVision") { cod_key1="U07_5S"; }
		else if (des_key1 == "Seguimiento SDI") { cod_key1="U07_6S"; }
		else if (des_key1 == "Seguimiento MAX") { cod_key1="U07_7S"; }
		else if (des_key1 == "Seguimiento Extensions") { cod_key1="U07_8S"; }
		else if (des_key1 == "Seguimiento Segmentación") { cod_key1="U07_9S"; }
		
		// IKANTEK
		// Ikantec
		else if (des_key1 == "Inviable - Ikantec") { cod_key1="UP16"; }
		else if (des_key1 == "Seguimiento Inviable - Ikantec") { cod_key1="UP16S"; }
		
		// OPDI
		// OPDI
		else if (des_key1 == "Inviable - OPDI") { cod_key1="UP170"; }
		
		// GESTIÓN DISUASORIOS
		// Gestión Disuasorios
		else if (des_key1 == "Gestión Disuasorios") { cod_key1="U08"; }
		else if (des_key1 == "Seguimiento Gestión Disuasorios") { cod_key1="U08S"; }
		
		// FIN KEYS UPSELL
		
		// INICIO COMMLOGS CV Y BO_FIDE
		
		// CV
		else if (des_key1 == "CVDOC") { cod_key1="CV10"; }
		
		// BO_FIDE
		// BOV
		else if (des_key1 == "Llamada externa") { cod_key1="BOV1"; }
		else if (des_key1 == "Gestion mail") { cod_key1="BOV2"; }
		else if (des_key1 == "XPRU") { cod_key1="BOV3"; }
		else if (des_key1 == "XVEN") { cod_key1="BOV8"; }
		else if (des_key1 == "Gestion Aviso") { cod_key1="BOV4"; }
		else if (des_key1 == "No procede") { cod_key1="BOV5"; }
		else if (des_key1 == "Valija") { cod_key1="BOV6"; }
		
		// FIN KEYS COMMLOGS CV Y BO_FIDE
		
		// INICIO COMMLOG CV_RBE_BOFIDE
		
		//  CV
		else if (des_key1 == "CITA") { cod_key1="CV1"; }
		else if (des_key1 == "DOC") { cod_key1="CV2"; }
		else if (des_key1 == "CONS") { cod_key1="CV3"; }
		else if (des_key1 == "MAIL") { cod_key1="CV4"; }
		else if (des_key1 == "NO_BO OC") { cod_key1="CV5"; }
		else if (des_key1 == "Cierre mantenimiento") { cod_key1="CV7"; }
		else if (des_key1 == "No apertura mantenimiento") { cod_key1="CV8"; }
		else if (des_key1 == "GESTION D.DIGITAL") { cod_key1="CV9"; }
		else if (des_key1 == "CVDOC") { cod_key1="CV10"; }
		else if (des_key1 == "CVBBDD") { cod_key1="CV20"; }
		else if (des_key1 == "Gestion 310") { cod_key1="CV310"; }
		
		// BO_FIDE
		// BOV
		else if (des_key1 == "Llamada externa") { cod_key1="BOV1"; }
		else if (des_key1 == "Gestion mail") { cod_key1="BOV2"; }
		else if (des_key1 == "XPRU") { cod_key1="BOV3"; }
		else if (des_key1 == "XVEN") { cod_key1="BOV8"; }
		else if (des_key1 == "Gestion Aviso") { cod_key1="BOV4"; }
		else if (des_key1 == "Recepción y reparto Valija") { cod_key1="BOV5"; }
		else if (des_key1 == "Valija") { cod_key1="BOV6"; }
		else if (des_key1 == "Reparto de avisos") { cod_key1="BOV9"; }
		else if (des_key1 == "Identificación Valija") { cod_key1="BOV011"; }
		
		// FIN KEYS COMMLOG CV_RBE_BOFIDE

		//KEYS de
		else if (des_key1 == "LEADSIO2-Llamada nueva") { cod_key1="LEADSIO2"; }

		else if (des_key1 == "LEADZTC3-Seguimiento") { cod_key1="LEADZTC4"; }
		else if (des_key1 == "LEADTMK3-Seguimiento") { cod_key1="LEADTMK4"; }
		else if (des_key1 == "LEADTZ3-Seguimiento") { cod_key1="LEADTZ4"; }
		else if (des_key1 == "LEAPFUM3-Seguimiento") { cod_key1="LEAPFUM4"; }
		else if (des_key1 == "LEADRFT3-Seguimiento") { cod_key1="LEADRFT4"; }
		else if (des_key1 == "LEADRFZ3-Seguimiento") { cod_key1="LEADRFZ4"; }
		else if (des_key1 == "LEADSIO2-Seguimiento") { cod_key1="LEADSIO2"; }
		else if (des_key1 == "LEADPTMK-Seguimiento") { cod_key1="LEADPTMK2"; }

		else if (des_key1 == "LEADPTMK-Improcedente") { cod_key1="LEAPTMK5"; }
		else if (des_key1 == "LEADSIO2-Improcedente") { cod_key1="LEADSIO5"; }
		else if (des_key1 == "LEADZTC3-Improcedente") { cod_key1="LETZTC5"; }
		else if (des_key1 == "LEADTMK3-Improcedente") { cod_key1="LETTMK5"; }
		else if (des_key1 == "LEADTZ3-Improcedente") { cod_key1="LETTZ5"; }
		else if (des_key1 == "LEAPTMK3-Improcedente") { cod_key1="LETPTMK5"; }
		else if (des_key1 == "LEAPFUM3-Improcedente") { cod_key1="LETPFUM5"; }
		else if (des_key1 == "LEADRFT3-Improcedente") { cod_key1="LETRFT5"; }
		else if (des_key1 == "LEADRFZ3-Improcedente") { cod_key1="LETRFZ5"; }

		else if (des_key1 == "LEADZTC3-Tramite") { cod_key1="LEADZTC4"; }
		else if (des_key1 == "LEADTMK3-Tramite") { cod_key1="LEADTMK4"; }
		else if (des_key1 == "LEADTZ3-Tramite") { cod_key1="LEADTZ4"; }
		else if (des_key1 == "LEAPFUM3-Tramite") { cod_key1="LEAPFUM4"; }
		else if (des_key1 == "LEADRFT3-Tramite") { cod_key1="LEADRFT4"; }
		else if (des_key1 == "LEADRFZ3-Tramite") { cod_key1="LEADRFZ4"; }
		else if (des_key1 == "LEADSIO2-Tramite") { cod_key1="LEADSIO2"; }
		else if (des_key1 == "LEADPTMK-Tramite") { cod_key1="LEADPTMK2"; }
		else if (des_key1 == "LEAPFUM3-Tramite") { cod_key1="LETPFUM5"; }
		else if (des_key1 == "LEADRFT3-Tramite") { cod_key1="LEADRFT4"; }
		else if (des_key1 == "LEADRFZ3-Tramite") { cod_key1="LEADRFZ4"; }

		return cod_key1;
	},

	obtener_key2_code: function (cod_key1, des_key2)
	{
		var cod_key2 = '';
		
		// Inbound
		// Llamada nueva + Seguimiento de llamadas
		if (cod_key1 == "LEADN" || cod_key1 == "LEADC" || cod_key1 == "LEADRTC" || cod_key1 == "LEADNSR" || cod_key1 == "LEADMCU" || cod_key1 == "LEADMCA"
		    || cod_key1 == "LEADMM"	|| cod_key1 == "LEADMO"	|| cod_key1 == "LEADRF" || cod_key1 == "LEADN2" || cod_key1 == "LEADC2" || cod_key1 == "LEADRTC2" || cod_key1 == "LEADMCU2"
			|| cod_key1 == "LEADMCA2" || cod_key1 == "LEADMM2" || cod_key1 == "LEADMO2" || cod_key1 == "LEADRF2" || cod_key1 == "LEADSI2"
			|| cod_key1 == "LEADTIB" || cod_key1 == "LEADTIB2" || cod_key1 == "LEADZTC2" || cod_key1 == "LEADTMK2" || cod_key1 == "LEADTZ2" || cod_key1 == "LEAPTMK2"
			|| cod_key1 == "LEAPFUM2" || cod_key1 == "LEADRFT2" || cod_key1 == "LEADRFZ2" || cod_key1 == "LEADMO2" || cod_key1 == "LEADZTC4"
			|| cod_key1 == "LEADTZ4" || cod_key1 == "LEADTMK4" || cod_key1 == "LEAPTMK4" || cod_key1 == "LEAPFUM4" || cod_key1 == "LEADRFT3" || cod_key1 == "LEADRFZ3"
		) {    //BLOQUE_KEY1
			if (des_key2 == "Grado") { cod_key2="L_ACEP1"; }
			else if (des_key2 == "Super") { cod_key2="L_ACEP2"; }
			else if (des_key2 == "High") { cod_key2="L_ACEP3"; }
			else if (des_key2 == "Medium") { cod_key2="L_ACEP4"; }
			else if (des_key2 == "Low") { cod_key2="L_ACEP5"; }
			else if (des_key2 == "Venta cruzada") { cod_key2="L_ACEP6"; }
			else if (des_key2 == "Me lo pienso") { cod_key2="L_PR"; }
			else if (des_key2 == "Asesoramiento técnico") { cod_key2="L_PR1"; }
			else if (des_key2 == "Ilocalizado") { cod_key2="L_ILOC"; }
			else if (des_key2 == "Pendiente de información") { cod_key2="L_PI"; }
			else if (des_key2 == "Venta traslado cese alquiler") { cod_key2="L_NOACE1"; }
			else if (des_key2 == "Falta de uso") { cod_key2="L_NOACE2"; }
			else if (des_key2 == "Insatisfecho servicio") { cod_key2="L_NOACE3"; }
			else if (des_key2 == "No pasa scoring") { cod_key2="L_NOACE4"; }
			else if (des_key2 == "Permanencia con otra compañía") { cod_key2="L_NOACE5"; }
			else if (des_key2 == "Inviabilidad técnica") { cod_key2="L_NOACE6"; }
			else if (des_key2 == "Alama operativa con SD") { cod_key2="L_NOACE7"; }
			else if (des_key2 == "Motivos económicos") { cod_key2="L_NOACE8"; }
			else if (des_key2 == "Reconexión futura") { cod_key2="L_NOACE9"; }
			else if (des_key2 == "Precio cuota") { cod_key2="L_NOACE10"; }
			else if (des_key2 == "Precio reconexión o upgrade") { cod_key2="L_NOACE11"; }
			else if (des_key2 == "Cliente desea baja LOPD") { cod_key2="L_NOAC12"; }
			else if (des_key2 == "Descontento con securitas direct") { cod_key2="L_NOACE13"; }
			else if (des_key2 == "Técnico") { cod_key2="TRAM1"; }
			else if (des_key2 == "Agenda") { cod_key2="TRAM2"; }
			else if (des_key2 == "Cliente") { cod_key2="TRAM3"; }
			else if (des_key2 == "Otros Trámites") { cod_key2="TRAM4"; }
			else if (des_key2 == "Corresponde a otro gestor") { cod_key2="TRAM5"; }
			else if (des_key2 == "Se realiza ANP/Traslado") { cod_key2="TRAM6"; }
		}
		if (cod_key1 == "LEADN5"	|| cod_key1 == "LEADC5"
		|| cod_key1 == "LEADRTC5" || cod_key1 == "LEADMCU5"	|| cod_key1 == "LEADMCA5" || cod_key1 == "LEADMM5"
		|| cod_key1 == "LEADMO5" || cod_key1 == "LEADRF5" || cod_key1 == "LEADTIB5"|| cod_key1 == "LEADSI5"	)
		{
			if (des_key2 == "Cliente duplicado") { cod_key2="L_NOPRO1"; }
			else if (des_key2 == "Tfno inexistente") { cod_key2="L_NOPRO2"; }
			else if (des_key2 == "Corresponde a Dpto Customer Care") { cod_key2="L_NOPRO5"; }
			else if (des_key2 == "Corresponde a Dpto Morosidad") { cod_key2="L_NOPRO6"; }
			else if (des_key2 == "Corresponde a Dpto Call Center") { cod_key2="L_NOPRO7"; }
			else if (des_key2 == "Corresponde a Dpto Agenda") { cod_key2="L_NOPRO8"; }
			else if (des_key2 == "Corresponde a Dpto Facturacion") { cod_key2="L_NOPRO9"; }
			else if (des_key2 == "Corresponde a Dpto Retencion") { cod_key2="L_NOPRO10"; }
			else if (des_key2 == "Corresponde a Dpto Upsell") { cod_key2="L_NOPRO11"; }
			else if (des_key2 == "Corresponde a Dpto DIY") { cod_key2="L_NOPRO12"; }
			else if (des_key2 == "Corresponde a otros dptos") { cod_key2="L_NOPRO13"; }
			else if (des_key2 == "Llamada en inglés - Corresponde a otro gestor") { cod_key2="L_NOPRO14"; }
			else if (des_key2 == "Improcedente") { cod_key2="L_NOPRO15"; }
		}
		if (cod_key1 == "LEADZTC" || cod_key1 == "LEADTMK" || cod_key1 == "LEADTZ"	|| cod_key1 == "LEADPTMK"
			|| cod_key1 == "LEAPFUM"|| cod_key1 == "LEADRFT"|| cod_key1 == "LEADRFZ"|| cod_key1 == "LEADTIP"
			|| cod_key1 == "LEADZTC3"|| cod_key1 == "LEADTMK3"|| cod_key1 == "LEADTZ3"|| cod_key1 == "LEAPTMK3"
			|| cod_key1 == "LEAPFUM3"|| cod_key1 == "LEADRFT3"|| cod_key1 == "LEADRFZ3"|| cod_key1 == "LEADTIP3"
			|| cod_key1 == "LEADZTC2"|| cod_key1 == "LEADTMK2"|| cod_key1 == "LEADTZ2"|| cod_key1 == "LEAPTMK2"
			|| cod_key1 == "LEAPFUM2"|| cod_key1 == "LEADRFT2"|| cod_key1 == "LEADRFZ2"|| cod_key1 == "LEADSIO2"
			|| cod_key1 == "LEADTIP2"|| cod_key1 == "LEADZTC4"|| cod_key1 == "LEADTZ4"|| cod_key1 == "LEADTMK4"
			|| cod_key1 == "LEAPTMK4"|| cod_key1 == "LEAPFUM4"|| cod_key1 == "LEADRFT4"|| cod_key1 == "LEADRFZ4"
			|| cod_key1 == "LEADTIP4"
		) {
			if (des_key2 == "Grado") { cod_key2="L_ACEP1"; }
			else if (des_key2 == "Super") { cod_key2="L_ACEP2"; }
			else if (des_key2 == "High") { cod_key2="L_ACEP3"; }
			else if (des_key2 == "Medium") { cod_key2="L_ACEP4"; }
			else if (des_key2 == "Low") { cod_key2="L_ACEP5"; }
			else if (des_key2 == "Venta cruzada") { cod_key2="L_ACEP6"; }
			else if (des_key2 == "Oferta especial competencia") { cod_key2="L_ACEP7"; }
			else if (des_key2 == "Incidencias servicio") { cod_key2="L_ACEP8"; }
			else if (des_key2 == "Económicos") { cod_key2="L_ACEP9"; }
			else if (des_key2 == "Permanencia (otras cias.)") { cod_key2="L_ACEP10"; }
			else if (des_key2 == "Otros") { cod_key2="L_ACEP11"; }
			else if (des_key2 == "Me lo pienso") { cod_key2="L_PR"; }
			else if (des_key2 == "Asesoramiento técnico") { cod_key2="L_PR1"; }
			else if (des_key2 == "Venta traslado cese alquiler") { cod_key2="L_NOACE1"; }
			else if (des_key2 == "Falta de uso") { cod_key2="L_NOACE2"; }
			else if (des_key2 == "Insatisfecho servicio") { cod_key2="L_NOACE3"; }
			else if (des_key2 == "No pasa scoring") { cod_key2="L_NOACE4"; }
			else if (des_key2 == "Permanencia con otra compañía") { cod_key2="L_NOACE5"; }
			else if (des_key2 == "Inviabilidad técnica") { cod_key2="L_NOACE6"; }
			else if (des_key2 == "Alama operativa con SD") { cod_key2="L_NOACE7"; }
			else if (des_key2 == "Cliente rechaza por motivos económicos personales no relacionados con el precio de cuota ni reconexión o upgrade") { cod_key2="L_NOACE8"; }
			else if (des_key2 == "Reconexión futura") { cod_key2="L_NOACE9"; }
			else if (des_key2 == "Precio cuota") { cod_key2="L_NOAC10"; }
			else if (des_key2 == "Precio reconexión o upgrade") { cod_key2="L_NOAC11"; }
			else if (des_key2 == "Cliente desea baja LOPD") { cod_key2="L_NOAC12"; }
			else if (des_key2 == "Descontento con securitas direct") { cod_key2="L_NOAC13"; }
			else if (des_key2 == "No Aplica") { cod_key2="L_NA"; }
			else if (des_key2 == "Pendiente de información") { cod_key2="L_PI"; }
			else if (des_key2 == "Ilocalizado") { cod_key2="L_ILOC"; }
			else if (des_key2 == "Teléfono erroneo") { cod_key2="L_TI"; }
			else if (des_key2 == "Técnico") { cod_key2="TRAM1"; }
			else if (des_key2 == "Agenda") { cod_key2="TRAM2"; }
			else if (des_key2 == "Cliente") { cod_key2="TRAM3"; }
			else if (des_key2 == "Otros Trámites") { cod_key2="TRAM4"; }
			else if (des_key2 == "Corresponde a otro gestor") { cod_key2="TRAM5"; }
		}
		if (cod_key1 == "LEADZTC" || cod_key1 == "LEADZTC5"	|| cod_key1 == "LEADTMK5"
		|| cod_key1 == "LEADTZ5" || cod_key1 == "LEAPTMK5" 	|| cod_key1 == "LEAPFUM5"
		|| cod_key1 == "LEADRFT5" || cod_key1 == "LEADRFZ5"	|| cod_key1 == "LETZTC5"
		|| cod_key1 == "LETTMK5" || cod_key1 == "LETTZ5" 	|| cod_key1 == "LETPTMK5"
		|| cod_key1 == "LETPFUM5"|| cod_key1 == "LETRFT5"|| cod_key1 == "LETRFZ5"
		|| cod_key1 == "LEADSIO5" || cod_key1 == "LEADTIP5"
		) {
			if (des_key2 == "Cliente duplicado") { cod_key2="L_NOPRO1"; }
			else if (des_key2 == "Tfno inexistente") { cod_key2="L_NOPRO2"; }
		}
		if (cod_key1.startsWith("U0")){
			if (des_key2 == "Acepta") { cod_key2="U2A_01"; }
			else if (des_key2 == "Acepta PR") { cod_key2="U2A_02"; }
			else if (des_key2 == "Competencia") { cod_key2="U2N_01"; }
			else if (des_key2 == "Upfront alto") { cod_key2="U2N_02"; }
			else if (des_key2 == "Cuota alta") { cod_key2="U2N_03"; }
			else if (des_key2 == "No interesa") { cod_key2="U2N_04"; }
			else if (des_key2 == "No necesita") { cod_key2="U2N_05"; }
			else if (des_key2 == "Reclama coste cero") { cod_key2="U2N_06"; }
			else if (des_key2 == "Lo quiere en una instalacion distinta") { cod_key2="U2N_07"; }
			else if (des_key2 == "Incidencia con Securitas Direct (robo, factura, problemas técnicos…)") { cod_key2="U2N_10"; }
			else if (des_key2 == "Ya tiene el producto ofrecido") { cod_key2="U2N_11"; }
			else if (des_key2 == "No usa alarma") { cod_key2="U2N_12"; }
			else if (des_key2 == "Oferta competencia") { cod_key2="U2N_13"; }
			else if (des_key2 == "Oferta Securitas Direct") { cod_key2="U2N_14"; }
			else if (des_key2 == "No colabora No escucha") { cod_key2="U2N_15"; }
			else if (des_key2 == "No quiere incremento en cuota") { cod_key2="U2N_16"; }
			else if (des_key2 == "Mala situacion economica") { cod_key2="U2N_17"; }
			else if (des_key2 == "Descontento con Securitas Direct") { cod_key2="U2N_18"; }
			else if (des_key2 == "Solicita baja en llamada") { cod_key2="U2N_19"; }
			else if (des_key2 == "Conocido sin coste") { cod_key2="U2N_20"; }
			else if (des_key2 == "Posible interes en el futuro") { cod_key2="U2N_21"; }
			else if (des_key2 == "Lo quiere en otra instalación") { cod_key2="U2N_22"; }
			else if (des_key2 == "Me lo pienso") { cod_key2="U2P_01"; }
			else if (des_key2 == "Lo consulto con 3º") { cod_key2="U2P_02"; }
			else if (des_key2 == "PI Pendiente de Información") { cod_key2="U2P_03"; }
			else if (des_key2 == "Pendiente (Ilocalizado entre 1 y 4 llamadas)") { cod_key2="U2P_04"; }
			else if (des_key2 == "Ilocalizado") { cod_key2="U2P_05"; }
			else if (des_key2 == "Inviable No cobertura") { cod_key2="U2I_01"; }
			else if (des_key2 == "Inviable No Internet") { cod_key2="U2I_02"; }
			else if (des_key2 == "Inviable Otros") { cod_key2="U2I_03"; }
			else if (des_key2 == "Técnico") { cod_key2="TRAM1"; }
			else if (des_key2 == "Agenda") { cod_key2="TRAM2"; }
			else if (des_key2 == "Cliente") { cod_key2="TRAM3"; }
			else if (des_key2 == "Otros Trámites") { cod_key2="TRAM4"; }
			else if (des_key2 == "Se realiza ANP/Traslado") { cod_key2="TRAM6"; }
			else if (des_key2 == "Mantenimiento de venta ya abierto") { cod_key2="U2E_12"; }
			else if (des_key2 == "Aviso de baja abierto") { cod_key2="U2E_13"; }
			else if (des_key2 == "Inviable en nivel 1") { cod_key2="U2E_14"; }
			else if (des_key2 == "Contrato en status Baja, XVEN, XCAN, XPRU") { cod_key2="U2E_15"; }
			else if (des_key2 == "Telefonos de contacto del cliente no operativos") { cod_key2="U2E_16"; }
			else if (des_key2 == "Cliente duplicado") { cod_key2="U2E_17"; }
			else if (des_key2 == "Error de codificación") { cod_key2="U2E_18"; }
			else if (des_key2 == "Venta ya gestionada") { cod_key2="U2E_19"; }
			else if (des_key2 == "Exclusión Sábana") { cod_key2="U2E_20"; }
			else if (des_key2 == "No se puede mejorar propuesta de Nivel 1") { cod_key2="U2E_21"; }
		}

		if (cod_key1 == "UI" || cod_key1 == "UI_1"	|| cod_key1 == "UI_05_6") {
			if (des_key2 == "Dept. ATC") { cod_key2="U2D_01"; }
			else if (des_key2 == "Dept. Comerciales") { cod_key2="U2D_02"; }
			else if (des_key2 == "Dept. ATT") { cod_key2="U2D_03"; }
			else if (des_key2 == "Dept. Grandes Cuentas") { cod_key2="U2D_04"; }
			else if (des_key2 == "Dept. Fidelización") { cod_key2="U2D_05"; }
			else if (des_key2 == "Técnico") { cod_key2="U2D_06"; }
			else if (des_key2 == "Otros Improcedente") { cod_key2="U2D_07"; }
			else if (des_key2 == "Rechaza información") { cod_key2="U2D_08"; }
			else if (des_key2 == "Reclama funcionamiento de la alarma") { cod_key2="U2D_09"; }
			else if (des_key2 == "Ya tiene mnto de ampliación abierto") { cod_key2="U2D_10"; }
			else if (des_key2 == "En gestión de fide") { cod_key2="U2D_11"; }
			else if (des_key2 == "Ya tiene producto ofrecido") { cod_key2="U2D_12"; }
			else if (des_key2 == "No toma decisiones") { cod_key2="U2D_13"; }
			else if (des_key2 == "Moroso") { cod_key2="U2D_14"; }

		}
		if (cod_key1 == "UP16" || cod_key1 == "UP16S") {
			if (des_key2 == "Acepta") { cod_key2="U2A_01"; }
			else if (des_key2 == "NoAcepta") { cod_key2="U2N_24"; }
			else if (des_key2 == "PR Me lo pienso") { cod_key2="U2P_01"; }
			else if (des_key2 == "PI Pendiente de Información") { cod_key2="U2P_03"; }
			else if (des_key2 == "Inviable Otros") { cod_key2="U2I_03"; }
			else if (des_key2 == "Trámite - Otros Trámites") { cod_key2="TRAM4"; }
		}
		if (cod_key1 == "UP170") {
			if (des_key2 == "OPDI - Sustitución") { cod_key2="UP171"; }
			else if (des_key2 == "OPDI - Ampliación") { cod_key2="UP172"; }
		}
		if (cod_key1 == "CV10") {
			if (des_key2 == "DOC PAPEL") { cod_key2="CV101"; }
			else if (des_key2 == "DOC DIGITAL") { cod_key2="CV102"; }
			else if (des_key2 == "GESTION ANEXOS") { cod_key2="CV103"; }
			else if (des_key2 == "GESTION MAIL") { cod_key2="CV104"; }
		}
		if (cod_key1 == "BOV1") {
			if (des_key2 == "PROCEDE") {cod_key2 = "BOV11";	}
			else if (des_key2 == "NO PROCEDE") { cod_key2="BOV12"; }
		}
		if (cod_key1 == "BOV2") {
			if (des_key2 == "Doc Valida") { cod_key2="BOV21"; }
			else if (des_key2 == "Doc No Valida") { cod_key2="BOV22"; }
			else if (des_key2 == "Doc Valida (anotamos en PTC)") { cod_key2="BOV23"; }
			else if (des_key2 == "Doc Valida Pte RTC") { cod_key2="BOV24"; }
			else if (des_key2 == "Doc No Valida Pte RTC") { cod_key2="BOV25"; }
			else if (des_key2 == "NO PROCEDE") { cod_key2="BOV26"; }
		}
		if (cod_key1 == "BOV4") {
			if (des_key2 == "PROCEDE") {cod_key2 = "BOV041";	}
			else if (des_key2 == "NO PROCEDE") { cod_key2="BOV042"; }
			else if (des_key2 == "GESTIONADO - MAL CODIFICADO") { cod_key2="BOV043"; }
		}
		if (cod_key1 == "BOV5") {
			if (des_key2 == "PROCEDE") {cod_key2 = "BOV51";	}
			else if (des_key2 == "NO PROCEDE") { cod_key2="BOV52"; }
		}
		if (cod_key1 == "BOV6") {
			if (des_key2 == "Doc Valida") { cod_key2="BOV61"; }
			else if (des_key2 == "Doc No Valida") { cod_key2="BOV62"; }
			else if (des_key2 == "Doc Valida (anotamos en PTC)") { cod_key2="BOV63"; }
			else if (des_key2 == "Doc Valida Pte RTC") { cod_key2="BOV64"; }
			else if (des_key2 == "Doc No Valida Pte RTC") { cod_key2="BOV65"; }
		}
		if (cod_key1 == "CV1") {
			if (des_key2 == "Acepta cita") { cod_key2="CV11"; }
			else if (des_key2 == "No acepta cita disponible") { cod_key2="CV12"; }
			else if (des_key2 == "Llamada seguimiento") { cod_key2="CV13"; }
			else if (des_key2 == "Aplazo cita") { cod_key2="CV14"; }
			else if (des_key2 == "Rechaza mnto") { cod_key2="CV15"; }
			else if (des_key2 == "Recuerdo cita") { cod_key2="CV16"; }
			else if (des_key2 == "Ilocalizado") { cod_key2="CV17"; }
			else if (des_key2 == "Cerrado") { cod_key2="CV18"; }
		}
		if (cod_key1 == "CV2") {
			if (des_key2 == "Envios nacex") { cod_key2="CV21"; }
			else if (des_key2 == "Incidencia en documentacion") { cod_key2="CV22"; }
			else if (des_key2 == "Reclamo Técnico") { cod_key2="CV23"; }
			else if (des_key2 == "Incid Docu Digital") { cod_key2="CV24"; }
			else if (des_key2 == "Revisión de mntto Upsell") { cod_key2="CV25"; }
			else if (des_key2 == "Revisión de mntto VTO financiación") { cod_key2="CV26"; }
			else if (des_key2 == "Revisión de mntto RBE") { cod_key2="CV27"; }
		}
		if (cod_key1 == "CV3") {
			if (des_key2 == "Incidencia en mnto") { cod_key2="CV31"; }
			else if (des_key2 == "Pendiente de respuesta de otro departamento") { cod_key2="CV32"; }
			else if (des_key2 == "Modificación del Pack") { cod_key2="CV33"; }
			else if (des_key2 == "Asesoramiento") { cod_key2="CV34"; }
		}
		if (cod_key1 == "CV4") {
			if (des_key2 == "Presupuestos") { cod_key2="CV41"; }
			else if (des_key2 == "Documentación") { cod_key2="CV43"; }
			else if (des_key2 == "Sepa y Ratificación") { cod_key2="CV44"; }
		}
		if (cod_key1 == "CV7") {
			if (des_key2 == "Rechaza venta") { cod_key2="CV71"; }
			else if (des_key2 == "Ilocalizable") { cod_key2="CV72"; }
			else if (des_key2 == "Aplazamiento cita por cliente") { cod_key2="CV73"; }
			else if (des_key2 == "Instalado en mantenimiento anterior") { cod_key2="CV74"; }
			else if (des_key2 == "Gestión Fidelización") { cod_key2="CV75"; }
			else if (des_key2 == "No procede apertura") { cod_key2="CV76"; }
			else if (des_key2 == "Abro solicitud de baja") { cod_key2="CV77"; }
		}
		if (cod_key1 == "CV8") {
			if (des_key2 == "No conforme oferta") { cod_key2="CV81"; }
			else if (des_key2 == "Requisitos incompletos para instalación") { cod_key2="CV82"; }
			else if (des_key2 == "Cliente pide asesoramiento") { cod_key2="CV83"; }
			else if (des_key2 == "Retomar cliente con duda") { cod_key2="CV84"; }
			else if (des_key2 == "Retomar más adelante") { cod_key2="CV85"; }
		}
		if (cod_key1 == "CV9") {
			if (des_key2 == "ILOCALIZABLE") { cod_key2="CV91"; }
			else if (des_key2 == "REPROGRAMO LLAMADA") { cod_key2="CV92"; }
			else if (des_key2 == "SOLICITO DATOS") { cod_key2="CV93"; }
			else if (des_key2 == "SOLUCION INCIDENCIA") { cod_key2="CV94"; }
			else if (des_key2 == "ACEPTA ENVIO DE ANEXO") { cod_key2="CV95"; }
			else if (des_key2 == "NO ACEPTA ENVIO DE ANEXO") { cod_key2="CV96"; }
			else if (des_key2 == "ANEXO RECIBIDO EN CV") { cod_key2="CV97"; }
			else if (des_key2 == "SE NIEGA A FIRMAR ANEXO") { cod_key2="CV98"; }
		}
		if (cod_key1 == "CV20") {
			if (des_key2 == "Carga bbdd Upsell") { cod_key2="CV201"; }
			else if (des_key2 == "Carga bbdd Vto Financiación") { cod_key2="CV202"; }
			else if (des_key2 == "Carga bbdd RB") { cod_key2="CV203"; }
			else if (des_key2 == "Reparto avisos Upsell") { cod_key2="CV204"; }
		}

		if (cod_key1 == "CV310") {
			if (des_key2 == "Ilocalizable") { cod_key2="CV3101"; }
			else if (des_key2 == "Rellamada") { cod_key2="CV3102"; }
			else if (des_key2 == "Envío Carta Baja") { cod_key2="CV3103"; }
			else if (des_key2 == "Apertura Mnto.") { cod_key2="CV3104"; }
			else if (des_key2 == "Envío Contrato") { cod_key2="CV3105"; }
			else if (des_key2 == "Cambio Status") { cod_key2="CV3106"; }
		}
		
		// Gestión mail ✔
		if (cod_key1 == "GESMAIL1") {
			if (des_key2 == "Ilocalizado") { cod_key2="GESM_IL1"; } 
			else if (des_key2 == "Pendiente de información") { cod_key2="GESM_PI1"; }
		}
		
		if (cod_key1 == "GESMAIL2") {
			if (des_key2 == "Ilocalizado") { cod_key2="GESM_IL2"; }
			else if (des_key2 == "Pendiente de información") { cod_key2="GESM_PI2"; }
		}
		
		return cod_key2;
	},
	
	obtener_key3_code : function (cod_key1, cod_key2, des_key3)
	{
		var cod_key3='';
		if (cod_key2 == "TRAM1" || cod_key2 == "TRAM2" ||cod_key2 == "TRAM3" ||cod_key2 == "TRAM4" ||cod_key2 == "TRAM5") {
			if (des_key3 == "Se soluciona/ Se transfiere llamada") { cod_key3="TRAMS_1"; }
			else if (des_key3 == "Se defiende venta") { cod_key3="TRAMS_2"; }
			else if (des_key3 == "Solicita apertura de mantenimiento") { cod_key3="TRAMS_3"; }
			else if (des_key3 == "Solicita cierre de mantenimiento") { cod_key3="CV7"; }
			else if (des_key3 == "Se soluciona/ Se transfiere llamada") { cod_key3="CV34"; }
			else if (des_key3 == "DOC") { cod_key3="CV2"; }
			else if (des_key3 == "CVC") { cod_key3="CVC"; }
			else if (des_key3 == "Abro solicitud de baja") { cod_key3="CV77"; }
			else if (des_key3 == "Asesoramiento") { cod_key3="CV34"; }
			else if (des_key3 == "No se autoriza a FSA a cerrar mantenimiento") { cod_key3="TRAMS_4"; }
			else if (des_key3 == "Sí se autoriza a FSA a cerrar mantenimiento") { cod_key3="TRAMS_5"; }
		}
		else if (cod_key2.startsWith("L_NOPRO")){
			if (des_key3 == "Genera lead util") { cod_key3="L_NOPROL"; }
		}
		if (cod_key2.startsWith("L_ACEP")) {
			if (des_key3 == "Referido") { cod_key3="L_RF"; }
			else if (des_key3 == "Finaliza permanencia") { cod_key3="L_RFP"; }
			else if (des_key3 == "Proviene de base anterior") { cod_key3="L_RFB"; }
		}
		if (cod_key2.startsWith("U2A_")) {
			if (des_key3 == "Se realiza NSR") { cod_key3="U3_01"; }
			else if (des_key3 == "No se realiza NSR") { cod_key3="U3_02"; }
			else if (des_key3 == "Se realiza NSR y No Cumple grado") { cod_key3="U3_03"; }
			else if (des_key3 == "No se realiza NSR y No Cumple grado") { cod_key3="U3_04"; }
		}
		if (cod_key2 == "CV101") {
			if (des_key3 == "DOC OK") { cod_key3="CV1011"; }
			else if (des_key3 == "DOC INCIDENCIA") { cod_key3="CV1012"; }
			else if (des_key3 == "SOLUCION INCIDENCIA") { cod_key3="CV1013"; }
		}
		if (cod_key2 == "CV102") {
			if (des_key3 == "DOC OK") { cod_key3="CV1021"; }
			else if (des_key3 == "DOC INCIDENCIA") { cod_key3="CV1022"; }
			else if (des_key3 == "SOLUCION INCIDENCIA") { cod_key3="CV1023"; }
		}
		if (cod_key2 == "CV103") {
			if (des_key3 == "DOC OK") { cod_key3="CV1031"; }
			else if (des_key3 == "DOC INCIDENCIA") { cod_key3="CV1032"; }
			else if (des_key3 == "SOLUCION INCIDENCIA") { cod_key3="CV1033"; }
		}

		if (cod_key2.startsWith("CV1")) {
			if (des_key3 == "NO EXISTE KEY 3") { cod_key3="CV000"; }
			else if (des_key3 == "Paso con agenda") { cod_key3="CV121"; }
			else if (des_key3 == "No tiene material") { cod_key3="CV141"; }
			else if (des_key3 == "No tiene luz/Adsl") { cod_key3="CV142"; }
			else if (des_key3 == "Quiere que se le llame otro dia") { cod_key3="CV143"; }
			else if (des_key3 == "Incumplimiento cliente (cliente no acude a la cita)") { cod_key3="CV144"; }
			else if (des_key3 == "Incumplimiento técnico (Incumplimiento por parte del técnico)") { cod_key3="CV151"; }
			else if (des_key3 == "No tiene material") { cod_key3="CV152"; }
			else if (des_key3 == "No tiene luz/Adsl") { cod_key3="CV153"; }
			else if (des_key3 == "Inviable") { cod_key3="CV154"; }
			else if (des_key3 == "No esta de acuerdo con precio") { cod_key3="CV155"; }
			else if (des_key3 == "Incidencia con agenda (retraso en cita, etc..)") { cod_key3="CV156"; }
			else if (des_key3 == "NO EXISTE KEY 3") { cod_key3="CV000"; }
			else if (des_key3 == "NO EXISTE KEY 3") { cod_key3="CV000"; }
			else if (des_key3 == "Cerrado por ilocalizado") { cod_key3="CV181"; }
		}

		if (cod_key2.startsWith("CV2")) {
			if (des_key3 == "Envio") { cod_key3="CV211"; }
			else if (des_key3 == "Incidencia") { cod_key3="CV212"; }
			else if (des_key3 == "Recogida") { cod_key3="CV213"; }
			else if (des_key3 == "Retorno") { cod_key3="CV214"; }
			else if (des_key3 == "Seguimiento") { cod_key3="CV215"; }
			else if (des_key3 == "Mails") { cod_key3="CV216"; }
			else if (des_key3 == "Carga Nacex") { cod_key3="CV217"; }
			else if (des_key3 == "Sepa/Ratificacion") { cod_key3="CV221"; }
			else if (des_key3 == "Doc sin recibir") { cod_key3="CV222"; }
			else if (des_key3 == "Cuota erronea/ Importe erroneo") { cod_key3="CV223"; }
			else if (des_key3 == "Datos Bancarios") { cod_key3="CV224"; }
			else if (des_key3 == "Falta Anexo III") { cod_key3="CV225"; }
			else if (des_key3 == "Datos Notariales") { cod_key3="CV226"; }
			else if (des_key3 == "Falta Firma") { cod_key3="CV227"; }
			else if (des_key3 == "Parte de Servicio") { cod_key3="CV228"; }
			else if (des_key3 == "Contrato Antiguo") { cod_key3="CV229"; }
			else if (des_key3 == "Documentacion") { cod_key3="CV231"; }
			else if (des_key3 == "Mnto no finalizado") { cod_key3="CV232"; }
			else if (des_key3 == "No tiene material") { cod_key3="CV233"; }
			else if (des_key3 == "Ingreso") { cod_key3="CV234"; }
			else if (des_key3 == "Anexo") { cod_key3="CV241"; }
			else if (des_key3 == "Firma") { cod_key3="CV242"; }
			else if (des_key3 == "Error Herramienta") { cod_key3="CV243"; }
			else if (des_key3 == "Resto") { cod_key3="CV244"; }
		}
		if (cod_key2.startsWith("CV3")) {
			if (des_key3 == "Cuota erronea/ Importe erroneo") { cod_key3="CV311"; }
			else if (des_key3 == "No esta titular") { cod_key3="CV312"; }
			else if (des_key3 == "Autorizo cargo en cuenta") { cod_key3="CV313"; }
			else if (des_key3 == "No tiene material") { cod_key3="CV314"; }
			else if (des_key3 == "No tiene luz/Adsl") { cod_key3="CV315"; }
			else if (des_key3 == "Inviable") { cod_key3="CV316"; }
			else if (des_key3 == "No esta de acuerdo con precio") { cod_key3="CV317"; }
			else if (des_key3 == "NO EXISTE KEY 3") { cod_key3="CV000"; }
			else if (des_key3 == "Vencimiento Financiación") { cod_key3="CV331"; }
			else if (des_key3 == "Prevención Residencial") { cod_key3="CV332"; }
			else if (des_key3 == "Prevención Negocio") { cod_key3="CV333"; }
			else if (des_key3 == "Upgrade Residencial") { cod_key3="CV334"; }
			else if (des_key3 == "Upgrade Negocio") { cod_key3="CV335"; }
			else if (des_key3 == "Cliente pide solo asesoramiento") { cod_key3="CV341"; }
			else if (des_key3 == "Motivado por tecnico") { cod_key3="CV342"; }
			else if (des_key3 == "Instalacion inviable") { cod_key3="CV343"; }
			else if (des_key3 == "No se ajusta a sus necesidades") { cod_key3="CV344"; }
		}
		if (cod_key2.startsWith("CV4")) {
			if (des_key3 == "Envio Cliente") { cod_key3="CV411"; }
			else if (des_key3 == "Envio Departamento") { cod_key3="CV412"; }
		}
		if (cod_key2.startsWith("CV7")) {
			if (des_key3 == "Cambio reiterado de cita") { cod_key3="CV711"; }
			else if (des_key3 == "Incumplimiento cita") { cod_key3="CV712"; }
			else if (des_key3 == "Robo") { cod_key3="CV714"; }
			else if (des_key3 == "Incidencia servicio/producto") { cod_key3="CV715"; }
			else if (des_key3 == "Cliente ausente (y no da opción a reagendar)") { cod_key3="CV716"; }
			else if (des_key3 == "Motivos económicos (ahora no puede asumir gastos)") { cod_key3="CV717"; }
			else if (des_key3 == "No fue el precio acordado en la venta") { cod_key3="CV718"; }
			else if (des_key3 == "No quiere instalar todos los dispositivos") { cod_key3="CV719"; }
			else if (des_key3 == "Necesita consultarlo") { cod_key3="CV720"; }
			else if (des_key3 == "No lo instala por estética") { cod_key3="CV721"; }
			else if (des_key3 == "No instala por asesoramiento del técnico") { cod_key3="CV722"; }
			else if (des_key3 == "No se adapta a sus necesidades") { cod_key3="CV723"; }
			else if (des_key3 == "NO EXISTE KEY3") { cod_key3="CV700"; }
			else if (des_key3 == "Cambio reiterado de cita") { cod_key3="CV771"; }
			else if (des_key3 == "Incumplimiento cita") { cod_key3="CV772"; }
			else if (des_key3 == "Competencia") { cod_key3="CV773"; }
			else if (des_key3 == "Mala venta") { cod_key3="CV774"; }
			else if (des_key3 == "Robo") { cod_key3="CV775"; }
			else if (des_key3 == "Incidencia servicio/producto") { cod_key3="CV776"; }
		}
		if (cod_key2.startsWith("CV9")) {
			if (des_key3 == "NO EXISTE KEY3") { cod_key3="CV910"; }
			else if (des_key3 == "CUOTA") { cod_key3="CV911"; }
			else if (des_key3 == "DATOS") { cod_key3="CV912"; }
			else if (des_key3 == "CONTRATO") { cod_key3="CV913"; }
			else if (des_key3 == "OK") { cod_key3="CV971"; }
			else if (des_key3 == "KO") { cod_key3="CV972"; }
			else if (des_key3 == "NO EXISTE KEY3") { cod_key3="CV910"; }
		}
		if (cod_key2 == "CV203") {
			if (des_key3 == "Incidencias") { cod_key3="CV2031"; }
			else if (des_key3 == "Informes") { cod_key3="CV2032"; }
			else if (des_key3 == "Carga BBDD") { cod_key3="CV2033"; }
		}
		return cod_key3;
	},

	obtener_key4_code : function (cod_key1, cod_key2, cod_key3, des_key4)
	{
		var cod_key4='';
		if (cod_key3 == "TRAMS_1" || cod_key3 == "TRAMS_2" || cod_key3 == "TRAMS_3" || cod_key3 == "CV7" || cod_key3 == "CV34") {
			if (des_key4 == "Cliente rechaza venta - Cambio reiterado de cita") { cod_key4="CV711"; }
			else if (des_key4 == "Cliente rechaza venta - Incumplimiento cita") { cod_key4="CV712"; }
			else if (des_key4 == "Cliente rechaza venta - Robo") { cod_key4="CV714"; }
			else if (des_key4 == "Cliente rechaza venta - Incidencia servicio/producto") { cod_key4="CV715"; }
			else if (des_key4 == "Cliente rechaza venta - Cliente ausente (y no da opción a reagendar)") { cod_key4="CV716"; }
			else if (des_key4 == "Cliente rechaza venta - Motivos económicos (ahora no puede asumir gastos)") { cod_key4="CV717"; }
			else if (des_key4 == "Cliente rechaza venta - No fue el precio acordado en la venta") { cod_key4="CV718"; }
			else if (des_key4 == "Cliente rechaza venta - No quiere instalar todos los dispositivos") { cod_key4="CV719"; }
			else if (des_key4 == "Cliente rechaza venta - Necesita consultarlo") { cod_key4="CV720"; }
			else if (des_key4 == "Cliente rechaza venta - No lo instala por estética") { cod_key4="CV721"; }
			else if (des_key4 == "Cliente rechaza venta - No instala por asesoramiento del técnico") { cod_key4="CV722"; }
			else if (des_key4 == "Cliente rechaza venta - No se adapta a sus necesidades") { cod_key4="CV723"; }
			else if (des_key4 == "Ilocalizable") { cod_key4="CV72"; }
			else if (des_key4 == "Aplazamiento cita por cliente") { cod_key4="CV73"; }
			else if (des_key4 == "No procede apertura") { cod_key4="CV76"; }
			else if (des_key4 == "Cliente pide solo asesoramiento") { cod_key4="CV341"; }
			else if (des_key4 == "Motivado por tecnico") { cod_key4="CV342"; }
			else if (des_key4 == "Instalacion inviable") { cod_key4="CV343"; }
			else if (des_key4 == "No se ajusta a sus necesidades") { cod_key4="CV344"; }
		}
		if (cod_key3 == "CV77" || cod_key3 == "TRAMS_2") {
			if (des_key4 == "Cliente rechaza venta - Cambio reiterado de cita") { cod_key4="CV711"; }
			else if (des_key4 == "Cliente rechaza venta - Incumplimiento cita") { cod_key4="CV712"; }
			else if (des_key4 == "Cliente rechaza venta - Robo") { cod_key4="CV714"; }
			else if (des_key4 == "Cliente rechaza venta - Incidencia servicio/producto") { cod_key4="CV715"; }
			else if (des_key4 == "Cliente rechaza venta - Cliente ausente (y no da opción a reagendar)") { cod_key4="CV716"; }
			else if (des_key4 == "Cliente rechaza venta - Motivos económicos (ahora no puede asumir gastos)") { cod_key4="CV717"; }
			else if (des_key4 == "Cliente rechaza venta - No fue el precio acordado en la venta") { cod_key4="CV718"; }
			else if (des_key4 == "Cliente rechaza venta - No quiere instalar todos los dispositivos") { cod_key4="CV719"; }
			else if (des_key4 == "Cliente rechaza venta - Necesita consultarlo") { cod_key4="CV720"; }
			else if (des_key4 == "Cliente rechaza venta - No lo instala por estética") { cod_key4="CV721"; }
			else if (des_key4 == "Cliente rechaza venta - No instala por asesoramiento del técnico") { cod_key4="CV722"; }
			else if (des_key4 == "Cliente rechaza venta - No se adapta a sus necesidades") { cod_key4="CV723"; }
			else if (des_key4 == "Ilocalizable") { cod_key4="CV72"; }
			else if (des_key4 == "Aplazamiento cita por cliente") { cod_key4="CV73"; }
			else if (des_key4 == "No procede apertura") { cod_key4="CV76"; }
			else if (des_key4 == "Cliente pide solo asesoramiento") { cod_key4="CV341"; }
			else if (des_key4 == "Motivado por tecnico") { cod_key4="CV342"; }
			else if (des_key4 == "Instalacion inviable") { cod_key4="CV343"; }
			else if (des_key4 == "No se ajusta a sus necesidades") { cod_key4="CV344"; }
		}
		if (cod_key3.startsWith("CV")) {
			if (des_key3 == "Revisión de mntto Upsell") { cod_key3="CV25"; }
			else if (des_key3 == "Revisión de mntto RBE") { cod_key3="CV27"; }
			else if (des_key3 == "Modificación del Pack") { cod_key3="CV33"; }
			else if (des_key3 == "Modificación mnto (cuando llama tec para quitar una llave, etc)") { cod_key3="CV35"; }
			else if (des_key3 == "Presupuestos") { cod_key3="CV41"; }
			else if (des_key3 == "Documentación (2 EN 1)") { cod_key3="CV45"; }
			else if (des_key3 == "SC") { cod_key3="CV10131"; }
		}
		if (cod_key3.startsWith("U3_")) {
			if (des_key3 == "llamada directa a Tienda Online Visitas") { cod_key3="U05_1701"; }
			else if (des_key3 == "llamada transferida  a Tienda Online Visitas") { cod_key3="U05_1702"; }
		}

		return cod_key4;
	}

}
