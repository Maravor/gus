'use strict';

const request = require('request'),
 querystring = require('querystring'),
        _ = require('lodash');
		
delete require.cache[require.resolve('./operacionesLib.js')];
var lib = require('./operacionesLib.js');

/**************
INFO
pasamos los datos de key1 y key1_senior para determinar el key1 final
***************/

function getKey3(req, res) {
	console.log('invocado fichero operacionesgetKey3.plugin.js');

	var array_salida = [];
	var cod_key3 = lib.obtener_key3_code(req.query.cod_key1, req.query.cod_key2, req.query.key3);

	array_salida.push({
			salida: cod_key3
		});
		
	res.jsonp(array_salida);	
}

module.exports = function (app, knex, auth) {
    app.route('/plugins/operacionesgetKey3')
            .get(auth.requiresLogin,  getKey3);
};